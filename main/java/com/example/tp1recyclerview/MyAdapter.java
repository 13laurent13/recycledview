package com.example.tp1recyclerview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_main, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        List<Pair<String, Integer>> pair = null;
        List<Pair<String, Integer>> tempo = null;
        Pair<String, Integer> paire = null;

        String[] nom = AnimalList.getNameArray();
        /*for(String n : nom)
        {
            Animal Name = AnimalList.getAnimal(n);
            String image = Name.getImgFile();
            try{
                int addr = R.drawable.class.getField(image).getInt(null);

                tempo.add(Pair.create(n, addr));
            }
            catch(Exception e){e.getMessage();}

        }*/

        Animal Name = AnimalList.getAnimal(nom[position]);
        String image = Name.getImgFile();
        try{
            int addr = R.drawable.class.getField(image).getInt(null);

            paire = Pair.create(nom[position], addr);
        }
        catch(Exception e){e.getMessage();}

        holder.display(paire);

    }

    @Override
    public int getItemCount() {
        return(AnimalList.getNameArray().length);
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private ImageView icon;

        public MyViewHolder(final View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            icon = (ImageView) itemView.findViewById(R.id.icon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), Detail.class);
                    String message = (String) name.getText();
                    intent.putExtra("MESSAGE", message);
                    view.getContext().startActivity(intent);
                }
            });
        }

        public void display(Pair<String, Integer> pair) {
            name.setText(pair.first);
            //name.setText(nom);
            icon.setImageResource(pair.second);
            //icon.setImageResource(add);
        }
    }

}