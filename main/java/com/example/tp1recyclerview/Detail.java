package com.example.tp1recyclerview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Detail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        Intent intent = getIntent();

        String animal = "";

        animal = intent.getStringExtra("MESSAGE");

        TextView nom = (TextView) findViewById(R.id.nom);
        nom.setText(animal);

        Animal Name = AnimalList.getAnimal(animal);
        String image = Name.getImgFile();
        int addr = getResources().getIdentifier(image, "drawable", getPackageName());

        ImageView img = (ImageView) findViewById(R.id.img);
        img.setImageResource(addr);

        TextView esperance = (TextView) findViewById(R.id.esperance2);
        esperance.setText(Name.getStrHightestLifespan());

        TextView gestation = (TextView) findViewById(R.id.gestation2);
        gestation.setText(Name.getStrGestationPeriod());

        TextView poidad = (TextView) findViewById(R.id.poidad2);
        poidad.setText(Name.getStrAdultWeight());

        TextView poidnaiss = (TextView) findViewById(R.id.poidnaiss2);
        poidnaiss.setText(Name.getStrBirthWeight());

        EditText statut = (EditText) findViewById(R.id.statut2);
        statut.setText(Name.getConservationStatus());
    }
}
